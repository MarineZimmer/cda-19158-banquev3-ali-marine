/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.Test;



public class TransactionsServiceBddTest {
    
    public TransactionsServiceBddTest() {
    }

    @Test
    public void testModificationSoldeCompte() throws IllegalArgumentException,SQLException {
       Connection conn = new ConnexionServiceBdd().connexionBdd();
        TransactionsServiceBdd transactionsServiceBdd = new TransactionsServiceBdd(conn);
       assertTrue(transactionsServiceBdd.modificationSoldeCompte("0000000001", "CC000001", "10"));
        new ConnexionServiceBdd().deconnexionBdd(conn);
    }

    @Test
    public void testInsertionTransaction() throws IllegalArgumentException,SQLException {
        Connection conn = new ConnexionServiceBdd().connexionBdd();
        TransactionsServiceBdd transactionsServiceBdd = new TransactionsServiceBdd(conn);
        assertTrue(transactionsServiceBdd.insertionTransaction(1, "0000000001", "CC000001", "10", null));
        assertTrue(transactionsServiceBdd.insertionTransaction(2, "0000000001", "CC000001", "10", null));
        assertTrue(transactionsServiceBdd.insertionTransaction(3, "0000000001", "CC000001", "10", "0000000002"));
        new ConnexionServiceBdd().deconnexionBdd(conn);
    }

    @Test
    public void testRechercheTransaction() {
    	
        Connection conn = new ConnexionServiceBdd().connexionBdd();
        TransactionsServiceBdd transactionsServiceBdd = new TransactionsServiceBdd(conn);
        assertNotNull(transactionsServiceBdd.rechercheTransaction(conn, "0000000001", "2010-10-10", "2020-01-03"));
        assertNull(transactionsServiceBdd.rechercheTransaction(conn, "0000000001", "20110-10-10", "2020-01-03"));
        assertNull(transactionsServiceBdd.rechercheTransaction(conn, "00000001", "20110-10-10", "2020-01-03"));
        new ConnexionServiceBdd().deconnexionBdd(conn);
    }
    
}
