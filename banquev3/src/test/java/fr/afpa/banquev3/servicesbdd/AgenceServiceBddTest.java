package fr.afpa.banquev3.servicesbdd;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;

import org.junit.jupiter.api.Test;

public class AgenceServiceBddTest {

	public AgenceServiceBddTest() {

	}

	@Test
	public void testCreerAgence() {
		 Connection conn = new ConnexionServiceBdd().connexionBdd();
	        AgenceServiceBdd agenceServiceBdd = new AgenceServiceBdd();
	        assertTrue(agenceServiceBdd.creationAgence(conn, "009", "AGENCE 9", "RONCQ"));
	        new ConnexionServiceBdd().deconnexionBdd(conn);
	}

}
