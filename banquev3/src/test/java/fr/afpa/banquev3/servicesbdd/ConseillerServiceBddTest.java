package fr.afpa.banquev3.servicesbdd;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

public class ConseillerServiceBddTest {

	public ConseillerServiceBddTest() {
		
	}
	
	@Test
	public void testCreationConseiller() throws IllegalArgumentException,SQLException {
        Connection conn = new ConnexionServiceBdd().connexionBdd();
        ConseillerServiceBdd conseillerServiceBdd = new ConseillerServiceBdd();
        assertTrue(conseillerServiceBdd.creationConseiller(conn, "CO0010", "CONS10", "Cons10", "2020-04-01", "cons10@gmail.com", "002"));
        new ConnexionServiceBdd().deconnexionBdd(conn);
    }
}
