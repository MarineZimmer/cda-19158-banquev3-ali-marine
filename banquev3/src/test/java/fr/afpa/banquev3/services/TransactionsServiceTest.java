/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;


public class TransactionsServiceTest {

    public TransactionsServiceTest() {
    }

    @Test
    public void testEffectuerTransaction() {
        TransactionsService transactionsService = new TransactionsService();
        assertTrue("test ajout", transactionsService.effectuerTransaction(1, "0000000001", "CC000001", "20", "", ""));
        assertFalse("test ajout montant negatif", transactionsService.effectuerTransaction(1, "0000000001", "CC000001", "-20", "", ""));
       assertFalse("test ajout mauvais idClient", transactionsService.effectuerTransaction(1, "0000000001", "CC000002", "20", "", ""));
        assertTrue("test retrait", transactionsService.effectuerTransaction(2, "0000000001", "CC000001", "20", "", ""));
        assertFalse("test retrait montant negatif", transactionsService.effectuerTransaction(2, "0000000001", "CC000001", "-20", "", ""));
        assertFalse("test retrait mauvais idClient", transactionsService.effectuerTransaction(2, "0000000001", "CC000002", "20", "", ""));
        assertTrue("test virement", transactionsService.effectuerTransaction(3, "0000000001", "CC000001", "20", "0000000001", "CC000001"));
        assertFalse("test virement pas destinataire", transactionsService.effectuerTransaction(3, "0000000001", "CC000001", "-20", "0000000001", ""));
        assertFalse("test virement mauvais idClient", transactionsService.effectuerTransaction(3, "0000000001", "CC000002", "20", "0000000001", "CC000001"));
        assertFalse("test transaction inconnue", transactionsService.effectuerTransaction(4, "0000000001", "CC000002", "20", "0000000001", "CC000001"));
        
    }
}
