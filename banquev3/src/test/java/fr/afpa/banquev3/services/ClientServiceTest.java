package fr.afpa.banquev3.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class ClientServiceTest {

	public ClientServiceTest() {

	}

	@Test
	public void testCreerClient() {
		ClientService clientService = new ClientService();
		assertTrue("test ajout", clientService.creationClient("CC000005", "CLIENT5", "Client5", "2020-01-04",
				"client5@gmail.com", "CO0001"));
		assertFalse("test ajout mauvais idClient", clientService.creationClient("CC5", "CLIENT5", "Client5",
				"2020-01-04", "client5@gmail.com", "CO0005"));
		assertFalse("test ajout mauvaise date de naissance", clientService.creationClient("CC000005", "CLIENT5",
				"Client5", "01-04-2020", "client5@gmail.com", "CO0005"));
		assertFalse("test ajout mauvais idConseiller", clientService.creationClient("CC000005", "CLIENT5", "Client5",
				"2020-01-04", "client5@gmail.com", "CO5"));
	}
	
	@Test
	public void testModifierDomiciliationClient() {
		ClientService clientService = new ClientService();
		assertFalse("test ajout mauvais idClient", clientService.creationClient("C04", "CLIENT4", "Client4",
				"2020-01-04", "client4@gmail.com", "CO0004"));
		assertFalse("test ajout mauvais idConseiller", clientService.creationClient("CC000004", "CLIENT4", "Client4",
				"2020-01-04", "client4@gmail.com", "CC4"));
	}
	
	@Test
	public void testModifierInformationClient() {
		ClientService clientService = new ClientService();
		assertFalse("test ajout mauvais idClient", clientService.creationClient("C", "CLIENT4", "Client4",
				"2020-01-04", "client4@gmail.com", "CO0004"));
		assertFalse("test ajout mauvaise date de naissance", clientService.creationClient("CC000005", "CLIENT5",
				"Client5", "01/04/2020", "client5@gmail.com", "CO0005"));
		
	}
        
        
        @Test
        public void testCreationFicheClient(){
            ClientService clientService = new ClientService();
             assertTrue(clientService.creationFicheClient("CC000001"));
            assertFalse(clientService.creationFicheClient("CC251"));
        }
        
        @Test
        public void testDesactivationClient(){
            ClientService clientService = new ClientService();
             assertTrue(clientService.desactiverClient("CC000002"));
            assertFalse(clientService.creationFicheClient("CC251"));
        }
        
        @Test
        public void testRechercheClient(){
            ClientService clientService = new ClientService();
            assertNotNull(clientService.rechercheClient("C", "CC", ""));
            assertEquals(0,clientService.rechercheClient("C", "CC0000025", "").getRowCount());
        }
	
}
