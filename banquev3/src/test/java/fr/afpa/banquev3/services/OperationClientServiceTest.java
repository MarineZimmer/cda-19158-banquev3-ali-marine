/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;


public class OperationClientServiceTest {
    
    public OperationClientServiceTest() {
    }

    @Test
    public void testTraitementChoixOperationClient() {
        OperationClientService ocs = new OperationClientService();
        assertTrue(ocs.traitementChoixOperationClient(0, "", "CC000001", "", "", "", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(1, "000000001", "CC000001", "", "", "", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(2, "000000001", "CC000001", "", "", "", "", ""));
        assertTrue(ocs.traitementChoixOperationClient(3, "0000000001", "CC000001", "", "", "", "2019-01-12", "2020-01-01"));
        assertTrue(ocs.traitementChoixOperationClient(4, "0000000001", "CC000001", "10", "", "", "", ""));
        assertTrue(ocs.traitementChoixOperationClient(5, "0000000001", "CC000001", "10", "", "", "", ""));
        assertTrue(ocs.traitementChoixOperationClient(6, "0000000001", "CC000001", "10", "0000000002", "CC000001", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(0, "", "CC004001", "", "", "", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(3, "0000000001", "CC000002", "", "", "", "2019-01-12", "2020-01-01"));
        assertFalse(ocs.traitementChoixOperationClient(4, "0000000001", "CC000002", "10", "", "", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(5, "0000000001", "CC000002", "10", "", "", "", ""));
        assertFalse(ocs.traitementChoixOperationClient(6, "0000000001", "CC000002", "10", "0000000002", "CC000001", "", ""));  
        assertFalse(ocs.traitementChoixOperationClient(7, "0000000001", "CC000002", "10", "0000000002", "CC000001", "", ""));  
        
    }
    
}
