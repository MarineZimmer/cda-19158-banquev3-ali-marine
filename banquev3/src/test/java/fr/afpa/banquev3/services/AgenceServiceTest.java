package fr.afpa.banquev3.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class AgenceServiceTest {

	public AgenceServiceTest() {
		
	}
	
	@Test
	public void testCreerAgence() {
		AgenceService agenceService = new AgenceService();
		assertTrue("test ajout", agenceService.creationAgence("005", "AGENCE 5", "TOURCOING"));
		assertFalse("test ajout mauvais codeAgence", agenceService.creationAgence("4", "AGENCE 4", "TOURCOING"));
		
	}
	
}
