package fr.afpa.banquev3.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class ConseillerServiceTest {

	public ConseillerServiceTest() {
	}

	@Test
	public void testCreerConseiller() {
		ConseillerService conseillerService = new ConseillerService();
		assertTrue("test ajout", conseillerService.creationConseiller("CO0007", "CONSEILLER1", "conseiller1",
				"2020-01-04", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvais idConseiller", conseillerService.creationConseiller("CO001", "CONSEILLER1",
				"conseiller1", "2020-01-04", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvaise date de naissance", conseillerService.creationConseiller("CO0001", "CONSEILLER1",
				"conseiller1", "04-01-2020", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvais codeAgence", conseillerService.creationConseiller("CO0001", "CONSEILLER1",
				"conseiller1", "2020-01-04", "conseiller1@gmail.com", "01"));
	}

}
