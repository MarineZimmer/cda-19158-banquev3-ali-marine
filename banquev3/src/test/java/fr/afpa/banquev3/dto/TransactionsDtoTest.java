/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;


public class TransactionsDtoTest {

    public TransactionsDtoTest() {
    }

    @Test
    public void testAlimentationCompte() {
        TransactionsDto transactionsDto = new TransactionsDto();
        assertTrue("alimentation correct", transactionsDto.alimentationCompte("0000000001", "CC000001", "100"));
        assertFalse("alimentation montant negatif", transactionsDto.alimentationCompte("0000000001", "CC000001", "-100"));
        assertFalse("alimentation mauvais id client", transactionsDto.alimentationCompte("0000000001", "CC000002", "100"));
        assertFalse("retrait mauvais compte", transactionsDto.virement("0000000004", "CC000001", "100", "0000000001", "CC000001"));

        assertFalse("alimentation mauvais compte", transactionsDto.alimentationCompte("0000000004", "CC000001", "100"));

    }

    @Test
    public void testRetraitArgent() {
        TransactionsDto transactionsDto = new TransactionsDto();
        assertTrue("retrait correct", transactionsDto.retraitArgent("0000000001", "CC000001", "100"));
        assertFalse("retrait montant negatif", transactionsDto.retraitArgent("0000000001", "CC000001", "-100"));
        assertFalse("retrait mauvais id client", transactionsDto.retraitArgent("0000000001", "CC000002", "100"));
        assertFalse("retrait mauvais compte", transactionsDto.retraitArgent("0000000004", "CC000001", "100"));

    }

    @Test
    public void testVirement() {
        TransactionsDto transactionsDto = new TransactionsDto();
        assertTrue("virement correct", transactionsDto.virement("0000000001", "CC000001", "100", "0000000001", "CC000001"));
        assertFalse("virement montant negatif", transactionsDto.virement("0000000001", "CC000001", "-100", "0000000001", "CC000001"));
        assertFalse("virement mauvais id client", transactionsDto.virement("0000000001", "CC000002", "100", "0000000001", "CC000001"));
        assertFalse("virement mauvais id client dest", transactionsDto.virement("0000000001", "CC000001", "100", "0000000001", "CC000002"));
        assertFalse("virement mauvais compte", transactionsDto.virement("0000000004", "CC000001", "100", "0000000001", "CC000001"));
       assertFalse("virement mauvais compte dest", transactionsDto.virement("0000000001", "CC000001", "100", "0000000001", "CC000004"));
    }

    @Test
    public void testRechercheTransaction() {
        TransactionsDto transactionsDto = new TransactionsDto();
        assertNotNull(transactionsDto.rechercheTransaction("0000000001", "2019-12-01", "2020-01-01"));
        assertNotNull(transactionsDto.rechercheTransaction("0000000001", "2019-12-01", ""));;
        assertNull(transactionsDto.rechercheTransaction("0000000001", "2020-612-01", ""));
        assertEquals(0,transactionsDto.rechercheTransaction("46545", "2019-12-01", "2019-05-01").size());
    }

}
