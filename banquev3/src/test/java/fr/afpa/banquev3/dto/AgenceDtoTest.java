package fr.afpa.banquev3.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class AgenceDtoTest {

	public AgenceDtoTest() {

	}

	@Test
	public void testCreationAgence() {
		AgenceDto agenceDto = new AgenceDto();
		assertTrue("test ajout", agenceDto.creationAgence("004", "AGENCE 4", "TOURCOING"));
		assertFalse("test ajout mauvais codeAgence", agenceDto.creationAgence("a", "AGENCE 4", "TOURCOING"));

	}
}
