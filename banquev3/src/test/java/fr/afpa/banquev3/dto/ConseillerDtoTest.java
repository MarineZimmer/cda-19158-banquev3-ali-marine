package fr.afpa.banquev3.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class ConseillerDtoTest {

	public ConseillerDtoTest() {
		
	}
	
	@Test
	 public void testCreationConseiller() {
        ConseillerDto conseillerDto = new ConseillerDto();
        assertTrue("test ajout", conseillerDto.creationConseiller("CO0008", "CONSEILLER1", "conseiller1",
				"2020-01-04", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvais idConseiller", conseillerDto.creationConseiller("C0001", "CONSEILLER1",
				"conseiller1", "2020-01-04", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvaise date de naissance", conseillerDto.creationConseiller("CO0001", "CONSEILLER1",
				"conseiller1", "2020/01/04", "conseiller1@gmail.com", "001"));
		assertFalse("test ajout mauvais codeAgence", conseillerDto.creationConseiller("CO0001", "CONSEILLER1",
				"conseiller1", "2020-01-04", "conseiller1@gmail.com", "AAA"));

    }
	
	
}
