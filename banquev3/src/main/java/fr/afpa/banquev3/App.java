package fr.afpa.banquev3;

import fr.afpa.banquev3.ihms.FenetrePrincipale;
import fr.afpa.banquev3.ihms.JDialogConnexion;


public class App 
{
    public static void main( String[] args )
    {
        
        FenetrePrincipale fenetrePrincipale = new FenetrePrincipale();
        JDialogConnexion jDialogConnexion = new JDialogConnexion(fenetrePrincipale, true);
        jDialogConnexion.setLocationRelativeTo(fenetrePrincipale);
        fenetrePrincipale.setLocationRelativeTo(fenetrePrincipale);
        jDialogConnexion.setVisible(true);
        fenetrePrincipale.setVisible(true);
        
    }
}
