/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.dto;

import fr.afpa.banquev3.entites.Administrateur;
import fr.afpa.banquev3.servicesbdd.AuthentificationServiceBdd;
import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuthentificationDto {

    /**
     * methode qui retourne l'administrateur authentifier, si le login est mot
     * de passe sont valide
     *
     * @param login : le login de l'administrateur
     * @param motDePasse : le mot de passe de l'administrateur
     * @return l'administrateur authentifier, null si l'authentification a
     * échouée
     */
    public Administrateur authentification(String login, String motDePasse) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        AuthentificationServiceBdd authentificationServiceBdd = new AuthentificationServiceBdd();
        Connection conn;
        conn = connexionServiceBdd.connexionBdd();
        ResultSet resultatRequete = authentificationServiceBdd.authentification(login, motDePasse, conn);
        Administrateur admin = null;
        try {
            if (resultatRequete.next()) {
                admin = new Administrateur(resultatRequete.getString(2), resultatRequete.getString(3), resultatRequete.getDate(5).toLocalDate(), resultatRequete.getString(4), resultatRequete.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AuthentificationDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        connexionServiceBdd.deconnexionBdd(conn);
        return admin;
    }

}
