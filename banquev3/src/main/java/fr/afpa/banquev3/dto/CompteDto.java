/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.dto;

import fr.afpa.banquev3.entites.Compte;
import fr.afpa.banquev3.servicesbdd.CompteServiceBdd;
import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CompteDto {

    private Connection conn;

    public CompteDto() {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        conn = connexionServiceBdd.connexionBdd();
    }

    /**
     * Dto qui fait le lien entre le service de recherche compte et la BDD
     * (recherche les nom dont le nom du propriètaire et l'id du propriètaire
     * et le numéro de compte commencent par les champs renseignés
     *
     * @param nom : le nom du propriètaire à rechercher
     * @param idClient : l'id du propriètaire à rechercher
     * @param numeroCompte : le numero de compte à rechercher
     * @return une liste de Compte correspondant à la recherche
     */
    public List<Compte> rechercheCompteClient(String nom, String idClient, String numeroCompte) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        CompteServiceBdd compteServiceBdd = new CompteServiceBdd();
        ResultSet res;
        List<Compte> listeCompte = new ArrayList<>();
        res = compteServiceBdd.rechercheCompteClient(conn, nom, idClient, numeroCompte);
        try {
            while (res.next()) {

                Compte compte = new Compte(res.getString(1), res.getString(7), res.getString(8), res.getFloat(2), res.getBoolean(3), res.getBoolean(4), res.getFloat(5));
                listeCompte.add(compte);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        conn = connexionServiceBdd.connexionBdd();
        connexionServiceBdd.deconnexionBdd(conn);
        return listeCompte;
    }
    
    /**
     * methode de création d'un nouveau compte (insertion du nouveau client en BDD)
     * 
     * @param numCompte que l'on souhaite ajouter
     * @param solde à l'ouverture du compte 
     * @param decouvert autorisé ou non
     * @param id_type : type de compte (courant, PEL, Livret A)
     * @param id_client auquel on souhaite ajouter le nouveau compte
     * @return true si la création a été effectuée avec succès, false sinon
     */
    
    public boolean creationCompte(String numCompte, String solde, String decouvert, String id_type, String id_client) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        CompteServiceBdd compteServiceBdd = new CompteServiceBdd();
        boolean resultat;
        conn = connexionServiceBdd.connexionBdd();
        resultat = compteServiceBdd.creationCompte(conn, numCompte, solde, decouvert, id_type, id_client);
        connexionServiceBdd.deconnexionBdd(conn);
        return resultat;
    }
}
