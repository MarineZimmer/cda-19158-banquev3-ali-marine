/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.dto;

import fr.afpa.banquev3.entites.Client;
import fr.afpa.banquev3.servicesbdd.ClientServiceBdd;
import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import fr.afpa.banquev3.servicesbdd.DesactiverCompteServiceBdd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDto {

    private Connection conn;

    public ClientDto() {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        conn = connexionServiceBdd.connexionBdd();
    }

    /**
     * methode de création d'un client (insertion du nouveau client en BDD)
     *
     * @param id : l'id du client(2 majuscules et 6 chiffres)
     * @param nom : le nom du client
     * @param prenom : le prenom du client
     * @param dateNaissance : la date de naissance du client
     * @param mail : le mail du client
     * @param idConseiller : l'identifiant de son conseiller atitré
     * @return true si la création a été effectuée avec succès, false sinon
     */
    public boolean creationClient(String id, String nom, String prenom, String dateNaissance, String mail, String idConseiller) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        ClientServiceBdd clientServiceBdd = new ClientServiceBdd(conn);
        boolean resultat;
        conn = connexionServiceBdd.connexionBdd();
        resultat = clientServiceBdd.creationClient(conn, id, nom, prenom, dateNaissance, mail, idConseiller);
        connexionServiceBdd.deconnexionBdd(conn);
        return resultat;
    }

    /**
     * Dto qui fait le lien entre le service de recherche clients et la BDD
     * (recherche les clients dont le nom et l'id et le numéro de compte
     * commencent par les champs renseignés
     *
     * @param nom : le nom à rechercher
     * @param idClient : l'id à rechercher
     * @param numeroCompte : le numero de compte à rechercher
     * @return une liste de Client correspondant à la recherche
     */
    public List<Client> rechercheClient(String nom, String idClient, String numeroCompte) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        ClientServiceBdd clientServiceBdd = new ClientServiceBdd(conn);
        ResultSet res;
        List<Client> listeClient = new ArrayList<>();
        res = clientServiceBdd.rechercheClient(conn, nom, idClient, numeroCompte);
        try {
            while (res.next()) {
                Client client = new Client(res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getDate(5).toLocalDate(), res.getBoolean(6), res.getString(7));
                listeClient.add(client);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        conn = connexionServiceBdd.connexionBdd();
        connexionServiceBdd.deconnexionBdd(conn);
        return listeClient;
    }

    /**
     * methode de modification de la domiciliation bancaire d'un client
     *
     * @param idClient à qui l'on souhaite modifier la domicikiation
     * @param idConseiller du nouveau conseiller auquel sera rattaché le client
     * @return true si l'update de la domiciliation du client à été éffectué
     * avec succès, false sinon
     */
    public boolean modifDomiciliationClient(String idClient, String idConseiller) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        ClientServiceBdd clientServiceBdd = new ClientServiceBdd(conn);
        boolean resultat = false;
        conn = connexionServiceBdd.connexionBdd();
        try {
            resultat = clientServiceBdd.modificationDomiciliationClient(conn, idClient, idConseiller);
        } catch (IllegalArgumentException | SQLException e) {
            Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, e);
        }
        connexionServiceBdd.deconnexionBdd(conn);
        return resultat;
    }
    
    /**
     * méthode de modification des informations du client
     * 
     * @param id du client auquel on souhaite modifier ces infos
     * @param nom du client à modifier les infos
     * @param prenom du client à modifier les infos
     * @param dateNaissance du client à modifier les infos
     * @param mail du client à modifier les infos
     * @return true si l'update de la mpdif des infos du client ont été éffectué
     * avec succès, false sinon
     */
    
    public boolean modifInformationClient(String id, String nom, String prenom, String dateNaissance, String mail) {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        ClientServiceBdd clientServiceBdd = new ClientServiceBdd(conn);
        boolean resultat = false;
        conn = connexionServiceBdd.connexionBdd();
        try {
            resultat = clientServiceBdd.modificationInformationClient(conn, id, nom, prenom, dateNaissance, mail);
        } catch (IllegalArgumentException e) {
            Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, e);
        }
        connexionServiceBdd.deconnexionBdd(conn);
        return resultat;
    }

    /**
     * Methode de désactivation d'un client, désactivation de tous ces comptes em même temps 
     * @param idClient : l'identifiant du client à désactiver
     * @return true si le client et ses compte on été désactiver, false sinon
     */
    public boolean desactiverClient(String idClient) {
        ClientServiceBdd clientServiceBdd = new ClientServiceBdd();
        DesactiverCompteServiceBdd desactiverCompteService = new DesactiverCompteServiceBdd(conn);
        try {
            conn.setAutoCommit(false);
            if(clientServiceBdd.desactiverClient(conn,idClient)){
                desactiverCompteService.desactiverEnsembleComptesClient(conn,idClient);
                conn.commit();
                return true;
            }else{
                conn.rollback();
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(ClientDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
     
    }

}
