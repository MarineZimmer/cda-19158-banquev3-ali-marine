/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.dto;

import fr.afpa.banquev3.entites.Operation;
import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import fr.afpa.banquev3.servicesbdd.TransactionsServiceBdd;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransactionsDto {

    private Connection conn;
    private final TransactionsServiceBdd transactionsServiceBdd;

    public TransactionsDto() {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        conn = connexionServiceBdd.connexionBdd();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        transactionsServiceBdd = new TransactionsServiceBdd(conn);
    }

    /**
     * methode d'alimentation d'un compte (modification du solde + insertion
     * nouvelle transaction)
     *
     * @param numeroCompte : numero du compte à alimenter
     * @param idClient : l'identifiant du client, le propriétaire du compte
     * @param montant : le montant à ajouter (positif)
     * @return true si la transaction a été effectuée avec succès, false sinon
     */
    public boolean alimentationCompte(String numeroCompte, String idClient, String montant) {

        try {
            if (transactionsServiceBdd.modificationSoldeCompte(numeroCompte, idClient, montant)) {
                transactionsServiceBdd.insertionTransaction(1, numeroCompte, idClient, montant, null);
                conn.commit();
                return true;
            } else {
                conn.rollback();
                return false;
            }
        } catch (SQLException | IllegalArgumentException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * methode retrait d'argent (modification du solde + insertion nouvelle
     * transaction)
     *
     * @param numeroCompte : numero du compte à retirer de l'argent
     * @param idClient : l'identifiant du client, le propriétaire du compte
     * @param montant : le montant à retirer (positif)
     * @return true si la transaction a été effectuée avec succès, false sinon
     */
    public boolean retraitArgent(String numeroCompte, String idClient, String montant) {
        try {
            if (transactionsServiceBdd.modificationSoldeCompte(numeroCompte, idClient, "-" + montant)) {
                transactionsServiceBdd.insertionTransaction(2, numeroCompte, idClient, montant, null);
                conn.commit();
                return true;
            } else {
                conn.rollback();
                return false;
            }

        } catch (SQLException | IllegalArgumentException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     *
     * methode d'alimentation d'un compte (modification du solde + insertion
     * nouvelle transaction)
     *
     * @param numeroCompte : numero du compte à retirer l'argent
     * @param idClient : l'identifiant du client, le propriétaire du compte à
     * retirer l'argent
     * @param montant : le montant à ajouter ou retirer (positif)
     * @param compteDestinataire : numero du compte à ajouter l'argent, le
     * compte destinataire
     * @param idClientDestinataire : l'identifiant du client destinataire, le
     * propriétaire du compte destinataire
     * @return true si la transaction a été effectuée avec succès, false sinon
     */
    public boolean virement(String numeroCompte, String idClient, String montant, String compteDestinataire, String idClientDestinataire) {

        try {
            if (transactionsServiceBdd.modificationSoldeCompte(numeroCompte, idClient, "-" + montant) && transactionsServiceBdd.modificationSoldeCompte(compteDestinataire, idClientDestinataire, montant)) {
                transactionsServiceBdd.insertionTransaction(3, numeroCompte, idClient, montant, compteDestinataire);
                conn.commit();
                return true;
            } else {
                conn.rollback();
                return false;
            }

        } catch (SQLException | IllegalArgumentException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * dto de recherche des transactions d'un compte d'un client, via
     * le numéro de compte dans une période donnée 
     *
     * @param numeroCompte : le numéro de compte
     * @param dateDebut : la date de début de la recherche
     * @param dateFin : la date de fin
     * @return une liste des opérations recherchées
     */
    public List<Operation> rechercheTransaction(String numeroCompte, String dateDebut, String dateFin) {

        ResultSet res;
        List<Operation> listeOperation = new ArrayList<>();
        res = transactionsServiceBdd.rechercheTransaction(conn, numeroCompte, dateDebut, dateFin);
        try {
            while (res.next()) {
                Operation operation;
                operation = new Operation(res.getString(2), res.getString(3), res.getString(4));
                listeOperation.add(operation);
            }
        } catch (SQLException | NullPointerException ex) {
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listeOperation;
    }

    @Override
    protected void finalize() {
        try {
            super.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        connexionServiceBdd.deconnexionBdd(conn);
    }

}
