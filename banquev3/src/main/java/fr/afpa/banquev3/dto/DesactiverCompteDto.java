package fr.afpa.banquev3.dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import fr.afpa.banquev3.servicesbdd.DesactiverCompteServiceBdd;

public class DesactiverCompteDto {

	private Connection conn;
    private final DesactiverCompteServiceBdd desactiverCompteServiceBdd;
    
    public DesactiverCompteDto() {
        ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
        conn = connexionServiceBdd.connexionBdd();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(DesactiverCompteDto.class.getName()).log(Level.SEVERE, null, ex);
        }
        desactiverCompteServiceBdd = new DesactiverCompteServiceBdd(conn);
    }
    
    /**
     * service de désactivation d'un compte 
	 * 
	 * @param numeroCompte que l'on souhaite désactiver
	 * @return true si la modification s'est effectué avec succès, false sinon
     */
    
    public boolean desactiverCompte(String numeroCompte) {
		boolean resultat;
        try {
                resultat = desactiverCompteServiceBdd.modificationCompteActif(numeroCompte);
                conn.commit();
                return resultat;

        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(TransactionsDto.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return false;
    }
    
}
