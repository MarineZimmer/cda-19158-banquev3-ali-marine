package fr.afpa.banquev3.dto;

import java.sql.Connection;

import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;
import fr.afpa.banquev3.servicesbdd.ConseillerServiceBdd;

public class ConseillerDto {

	private Connection conn;

	public ConseillerDto() {
		ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
		conn = connexionServiceBdd.connexionBdd();
	}

	/**
	 * methode de création d'un nouveau conseiller
	 * 
	 * @param @param        id du nouveau conseiller (CO et 4 chiffres)
	 * @param nom           du nouveau conseiller
	 * @param prenom        du nouveau conseiller
	 * @param dateNaissance du nouveau conseiller
	 * @param mail          du nouveau conseiller
	 * @param codeAgence    : agence à laquelle le nouveau conseiller va être
	 *                      rattaché
	 * @return true si la création a été effectuée avec succès, false sinon
	 */

	public boolean creationConseiller(String id, String nom, String prenom, String dateNaissance, String mail,
			String codeAgence) {
		ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
		ConseillerServiceBdd conseillerServiceBdd = new ConseillerServiceBdd();
		boolean resultat;
		conn = connexionServiceBdd.connexionBdd();
		resultat = conseillerServiceBdd.creationConseiller(conn, id, nom, prenom, dateNaissance, mail, codeAgence);
		connexionServiceBdd.deconnexionBdd(conn);
		return resultat;
	}
}
