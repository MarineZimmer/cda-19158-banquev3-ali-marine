package fr.afpa.banquev3.dto;

import java.sql.Connection;

import fr.afpa.banquev3.servicesbdd.AgenceServiceBdd;
import fr.afpa.banquev3.servicesbdd.ConnexionServiceBdd;

public class AgenceDto {


	private Connection conn;

	public AgenceDto() {
		ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
		conn = connexionServiceBdd.connexionBdd();
	}
	
	/**
	 * méthode créant une nouvelle agence
	 * 
	 * @param codeAgence de la nouvelle agence (3 chiffres)
	 * @param nomAgence de la nouvelle agence
	 * @param adresseAgence de la nouvelle agence
	 * @return  true si la création a été effectuée avec succès, false sinon
	 */
	
	public boolean creationAgence(String codeAgence, String nomAgence, String adresseAgence) {
		ConnexionServiceBdd connexionServiceBdd = new ConnexionServiceBdd();
		AgenceServiceBdd agenceServiceBdd = new AgenceServiceBdd();
		boolean resultat;
		conn = connexionServiceBdd.connexionBdd();
		resultat = agenceServiceBdd.creationAgence(conn, codeAgence, nomAgence, adresseAgence);
		connexionServiceBdd.deconnexionBdd(conn);
		return resultat;
	}
}
