/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransactionsServiceBdd {

    private final String REQUETE_MODIFICATION_SOLDE_COMPTE = "UPDATE compte SET solde=solde+? WHERE id_client=? AND numero_compte=? AND actif;";
    private final String REQUETE_INSERTION_TRANSACTION = "INSERT INTO effectuer_transaction(id_client,numero_compte,id_type_operation,date_operation,montant,compte_destinataire) VALUES(?, ?,?,now(),?,?);";
    private static final String REQUETE_RECHERCHE_OPERATION = "select et.numero_compte, et.date_operation,t.libelle, (case when (et.id_type_operation = '2' or (et.id_type_operation = '3' and numero_compte=?)) then -et.montant else et.montant end), et.compte_destinataire "
            + "                                                 from effectuer_transaction et inner join type_operation t on et.id_type_operation = t.id_type_operation "
            + "                                                     where (numero_compte = ? or compte_destinataire = ?) and date_operation between ? and ?;";
    
    private PreparedStatement prepStatModifSolde;
    private PreparedStatement prepStatInsertTransaction;
    private PreparedStatement prepStatRechOperation;

    public TransactionsServiceBdd(Connection conn) {
        try {
            prepStatModifSolde = conn.prepareStatement(REQUETE_MODIFICATION_SOLDE_COMPTE);
            prepStatInsertTransaction = conn.prepareStatement(REQUETE_INSERTION_TRANSACTION);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionsServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * service bdd de modification du solde d'un compte (update)
     *
     * @param numeroCompte : numero du compte à modifier le solde
     * @param idClient : l'identifiant du propriétaire du compte à modifier le
     * solde
     * @param montant : le montant à ajouter (positif : pour un ajout, négatif
     * pour un retrait=
     * @return true si l'update du solde su compte à été éffectué avec succès,
     * false sinon
     * @throws SQLException : renvoie une exception si il y a eu une exception
     * lors de l'éxécution de la requete
     */
    public boolean modificationSoldeCompte(String numeroCompte, String idClient, String montant) throws SQLException,IllegalArgumentException{
        prepStatModifSolde.setFloat(1, Float.valueOf(montant));
        prepStatModifSolde.setString(2, idClient);
        prepStatModifSolde.setString(3, numeroCompte);
        return prepStatModifSolde.executeUpdate() == 1;
    }

    /**
     * service bdd d'insertion d'une nouvelle transaction
     *
     * @param typeTransaction : 1: ajout,2 :retrait,3: virement
     * @param numeroCompte : numero du compte à l'origine de la transaction
     * @param idClient : id du client propriétaire du compte à l'origine de la
     * transaction
     * @param montant : le montant de la transaction
     * @param compteDestinataire : numero de compte destinataire du virement
     * @return true si l'insertion a été effectuer avec succès, false sinon
     * @throws SQLException renvoie une exception si il y a eu une exception
     * lors de l'éxécution de la requete
     */
    public boolean insertionTransaction(int typeTransaction, String numeroCompte, String idClient, String montant, String compteDestinataire)  throws SQLException,IllegalArgumentException{
        prepStatInsertTransaction.setString(1, idClient);
        prepStatInsertTransaction.setString(2, numeroCompte);
        prepStatInsertTransaction.setInt(3, typeTransaction);
        prepStatInsertTransaction.setFloat(4, Float.valueOf(montant));
        prepStatInsertTransaction.setString(5, compteDestinataire);
        return prepStatInsertTransaction.executeUpdate() == 1;

    }

    /**
     * Service de recherche des transactions concernant un compte durant une période
     * @param conn : la connexioon à la BDD
     * @param numeroCompte : le numéro dont on souhaite récupérer les transactions
     * @param dateDebut : la date de début
     * @param dateFin : la date de fin
     * @return  un ResultSet avec le résultat de la recherche
     */
    public ResultSet rechercheTransaction(Connection conn, String numeroCompte, String dateDebut, String dateFin) {
        try {
            prepStatRechOperation = conn.prepareStatement(REQUETE_RECHERCHE_OPERATION, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prepStatRechOperation.setString(1, numeroCompte);
            prepStatRechOperation.setString(2, numeroCompte);
            prepStatRechOperation.setString(3, numeroCompte);
            prepStatRechOperation.setDate(4, Date.valueOf(dateDebut));
            if(!"".equals(dateFin)){
                prepStatRechOperation.setDate(5, Date.valueOf(dateFin));
            }else{
                 prepStatRechOperation.setDate(5, Date.valueOf(LocalDate.now()));
            }
            return prepStatRechOperation.executeQuery();
        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(TransactionsServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

}
