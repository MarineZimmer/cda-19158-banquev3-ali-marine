package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AgenceServiceBdd {

	private PreparedStatement prepStatAgence;
    private static final String REQUETE_NOUV_AGENCE = "insert into agence values (?, ?, ?, 'ADM00')";
    
    /**
     * méthode d'insertion d'une nouvelle agence dans la base de donnée
     * 
     * @param codeAgence de la nouvelle agence (3 chiffres)
	 * @param nomAgence de la nouvelle agence
	 * @param adresseAgence de la nouvelle agence
     * @return true si l'insertion a été effectuée avec succès, false sinon
     */
    
    public boolean creationAgence(Connection conn, String codeAgence, String nomAgence, String adresseAgence) {
        boolean resultat = false;
        try {
        	prepStatAgence = conn.prepareStatement(REQUETE_NOUV_AGENCE);
        	prepStatAgence.setString(1, codeAgence);
        	prepStatAgence.setString(2, nomAgence);
        	prepStatAgence.setString(3, adresseAgence);
        	prepStatAgence.executeUpdate();
            resultat = true;
        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (prepStatAgence != null) {
                try {
                	prepStatAgence.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultat;
    }
}
