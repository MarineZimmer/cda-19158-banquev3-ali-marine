/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnexionServiceBdd {

    /**
     * service de connexion à une bdd, info de connexion se trouve dans un
     * fichier properties dont le chemin se situe dans la variable
     * d'environnement configBDD (user, password,url) ou le fichier se situe à la racine du dossier
     * d'execution de l'application
     *
     * @return un objet connexion representant une connexion à la bdd
     */
    public Connection connexionBdd() {

        //  charge les properties de la connexion bdd via le chemin du fichier de la variable environnement configBDD ou le fichier se situe à la racine du dossier d'execution de l'application
        Properties props = new Properties();
        FileInputStream in = null;
        Connection conn = null;
        try {
            if (System.getenv("configBDD") != null) {
                in = new FileInputStream(System.getenv("configBDD"));
            } else {
                in = new FileInputStream("configBdd.properties");
            }
            props.load(in);

        }catch (IOException ex) {
             Logger.getLogger(ConnexionServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
            try {
                in = new FileInputStream("configBdd.properties");
            } catch (FileNotFoundException ex1) {
                Logger.getLogger(ConnexionServiceBdd.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(ConnexionServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        //connexion à la base de données
        try {
            conn = DriverManager.getConnection(props.getProperty("url"), props);
        } catch (SQLException ex) {
            Logger.getLogger(ConnexionServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        }

        return conn;
    }

    /**
     * service de deconnexion à une bdd
     *
     * @param conn : la connexion à deconnecter
     * @return true si la deconnexion est réussite, false sinon
     */
    public boolean deconnexionBdd(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(ConnexionServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }
}
