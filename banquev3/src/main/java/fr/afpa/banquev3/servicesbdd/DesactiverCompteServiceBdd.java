package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DesactiverCompteServiceBdd {


    private final String REQUETE_MODIFICATION_COMPTE_ACTIF = "UPDATE compte co SET actif=false from client c where c.id_client=co.id_client and co.numero_compte=?  and ((c.actif=true and co.id_type!=1) OR c.actif=false);";
    private final String REQUETE_DESACTIVATION_ENSENBLE_COMPTES_CLIENT = "UPDATE compte SET actif=false WHERE id_client=?";
	private PreparedStatement prepStatModifCompteActif;
         private PreparedStatement    prepStatDesactiveCompte;

    public DesactiverCompteServiceBdd() {
    }
	
        
        
	 public DesactiverCompteServiceBdd(Connection conn) {
	        try {
	            prepStatModifCompteActif = conn.prepareStatement(REQUETE_MODIFICATION_COMPTE_ACTIF);
	        } catch (SQLException ex) {
	            Logger.getLogger(DesactiverCompteServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
	 /**
	  * service bdd de modification d'un compte actif en un compte inactif
	  * 
	  * @param numeroCompte que l'on souhaite modifier
	  * @return true si l'update de l'activité du compte à été éffectué avec succès, false sinon
	  * @throws SQLException 
	  * @throws IllegalArgumentException
	  */
	 
	 public boolean modificationCompteActif(String numeroCompte) throws SQLException,IllegalArgumentException{
	        prepStatModifCompteActif.setString(1, numeroCompte);
	        return prepStatModifCompteActif.executeUpdate() == 1;
	    }
         
         /**
          * service de désactivation de l'ensemble des comptes d'un client
          * @param conn : la connection à la BDD
          * @param idClient : l'identifiant du client 
          * @return true si l'ensemble des comptes ont été désactivés 
          * @throws SQLException
          * @throws IllegalArgumentException 
          */
         public boolean desactiverEnsembleComptesClient(Connection conn, String idClient) throws SQLException,IllegalArgumentException{
	    
             prepStatDesactiveCompte = conn.prepareStatement(REQUETE_DESACTIVATION_ENSENBLE_COMPTES_CLIENT);
             prepStatDesactiveCompte.setString(1, idClient);
	        System.out.println(prepStatDesactiveCompte.executeUpdate());
                return true;
         }
         
}