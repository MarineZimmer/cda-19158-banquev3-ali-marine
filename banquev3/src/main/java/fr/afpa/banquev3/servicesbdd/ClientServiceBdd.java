/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientServiceBdd {

	private PreparedStatement prepStatClient;
	private static final String REQUETE_NOUV_CLIENT = "insert into client values (?, ?, ?,?,?,true, ?)";
	private static final String REQUETE_RECHERCHE_CLIENT = "SELECT distinct c.* FROM client c LEFT OUTER JOIN compte co on c.id_client=co.id_client WHERE nom LIKE ? AND c.id_client LIKE ? AND COALESCE(numero_compte,'') LIKE ? ";
	private final String REQUETE_MODIFICATION_DOMICILIATION_CLIENT = "UPDATE client SET id_emp=? WHERE id_client=?;";
	
    private final String REQUETE_DESACTIVER_CLIENT= "UPDATE client set actif=false where id_client=?";
    public ClientServiceBdd() {
    }

    public ClientServiceBdd(Connection conn) {
		try {
			prepStatClient = conn.prepareStatement(REQUETE_MODIFICATION_DOMICILIATION_CLIENT);
		} catch (SQLException ex) {
			Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
    
    
    /**
     * service d'insertion du nouveau client en BDD
     *
     * @param conn : la connexion à la bdd
     * @param id : l'id du nouveau client(2 majuscules et 6 chiffres)
     * @param nom : le nom du nouveau client
     * @param prenom : le prenom du nouveau client
     * @param dateNaissance : la date de naissance du nouveau client
     * @param mail : le mail du nouveau client
     * @param idConseiller : l'identifiant de son conseiller atitré
     * @return true si l'insertion a été effectuée avec succès, false sinon
     */
    public boolean creationClient(Connection conn, String id, String nom, String prenom, String dateNaissance, String mail, String idConseiller) {
        boolean resultat = false;
        try {
            prepStatClient = conn.prepareStatement(REQUETE_NOUV_CLIENT);
            prepStatClient.setString(1, id);
            prepStatClient.setString(2, nom);
            prepStatClient.setString(3, prenom);
            prepStatClient.setObject(5, Date.valueOf(dateNaissance));
            prepStatClient.setString(4, mail);
            prepStatClient.setString(6, idConseiller);
            prepStatClient.executeUpdate();
            resultat = true;
        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (prepStatClient != null) {
                try {
                    prepStatClient.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultat;
    }

	/**
	 * service bdd qui interooge la BDD pour une recherche clients dont le nom et
	 * l'id et le numéro de compte commencent par les champs renseignés en
	 * paramètres
	 *
	 *
	 * @param conn         : la connection à la BDD
	 * @param nom          : le nom à rechercher
	 * @param idClient     : l'id à rechercher
	 * @param numeroCompte : le numero de compte à rechercher
	 * @return un ResultSet correspondant au resultat de la recherche
	 */
	public ResultSet rechercheClient(Connection conn, String nom, String idClient, String numeroCompte) {

		try {
			prepStatClient = conn.prepareStatement(REQUETE_RECHERCHE_CLIENT, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			prepStatClient.setString(1, nom + "%");
			prepStatClient.setString(2, idClient + "%");
			prepStatClient.setString(3, numeroCompte + "%");
			return prepStatClient.executeQuery();
		} catch (SQLException ex) {
			Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;

	}

	

	/**
	 * service bdd de modification de la domiciliation bancaire d'un client
	 * 
	 * @param conn
	 * @param idClient     à qui l'on souhaite modifier la domiciliation
	 * @param idConseiller du nouveau conseiller auquel sera rattaché le client
	 * @return true si l'update de la domiciliation du client à été éffectué avec
	 *         succès, false sinon
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */

	public boolean modificationDomiciliationClient(Connection conn, String idClient, String idConseiller)
			throws SQLException, IllegalArgumentException {

		prepStatClient.setString(1, idConseiller);
		prepStatClient.setString(2, idClient);

		return prepStatClient.executeUpdate() == 1;
	}

	/**
	 * service bdd de modification des informations du client
	 * 
	 * @param conn
	 * @param id            du client auquel on souhaite modifier ces infos
	 * @param nom           du client à modifier les infos
	 * @param prenom        du client à modifier les infos
	 * @param dateNaissance du client à modifier les infos
	 * @param mail          du client à modifier les infos
	 * @return true si l'update de la domiciliation du client à été éffectué avec
	 *         succès, false sinon
	 */

	public boolean modificationInformationClient(Connection conn, String id, String nom, String prenom,
			String dateNaissance, String mail) {
		boolean resultat = false;
                StringBuilder requete = new StringBuilder("UPDATE client ");
                if (!"".equals(nom)) {
				requete.append(" SET nom=?,");
			} 
			if (!"".equals(prenom)) {
				requete.append(" SET prenom=?,");
			} 
			if (!"".equals(mail)) {
				requete.append(" SET mail=?,");
			} 
			if (!"".equals(dateNaissance)) {
				requete.append(" SET date_naissance=?");
			}
                        if(requete.charAt(requete.length()-1)==','){
                            requete.deleteCharAt(requete.length()-1);
                        }
                        requete.append(" WHERE id_client=?;");

		try {
                        int indice=1;
			prepStatClient = conn.prepareStatement(requete.toString());
			if (!"".equals(nom)) {
                            prepStatClient.setString(indice++, nom);
			}
			if (!"".equals(prenom)) {
				prepStatClient.setString(indice++, prenom);
			}
			if (!"".equals(mail)) {
				prepStatClient.setString(indice++, mail);
			}
			if (!"".equals(dateNaissance)) {
				prepStatClient.setDate(indice++, Date.valueOf(dateNaissance));
			}
			prepStatClient.setString(indice++, id);
                        
			resultat = prepStatClient.executeUpdate()==1;
		} catch (SQLException | IllegalArgumentException ex) {
			Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (prepStatClient != null) {
				try {
					prepStatClient.close();
				} catch (SQLException ex) {
					Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return resultat;
	}

        /**
         * service de désactivation d'un client
         * @param conn : la connection à la BDD
         * @param idClient : l'identifiant du client à désactiver
         * @return true si la désactivation à été effectuée
         */
    public boolean desactiverClient(Connection conn, String idClient) {
        
        try {
            prepStatClient = conn.prepareStatement(REQUETE_DESACTIVER_CLIENT);
            prepStatClient.setString(1, idClient);
            return prepStatClient.executeUpdate() == 1;
                    } catch (SQLException ex) {
            Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
      }

}
