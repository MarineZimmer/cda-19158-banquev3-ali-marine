package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConseillerServiceBdd {

	private PreparedStatement prepStatConseiller;
    private static final String REQUETE_NOUV_CONSEILLER = "insert into employe values (?, ?, ?, ?, ?, 2,?)";


	/**
     * méthode d'insertion d'un nouveau conseiller en bdd 
     * 
     * @param conn : connection à la bdd
     * @param id  du nouveau conseiller (CO et 4 chiffres)
     * @param nom du nouveau conseiller
     * @param prenom du nouveau conseiller
     * @param dateNaissance du nouveau conseiller
     * @param mail du nouveau conseiller
     * @param codeAgence : agence à laquelle le nouveau conseiller va être rattaché
     * @return true si l'insertion a été effectuée avec succès, false sinon
     */
    public boolean creationConseiller(Connection conn, String id, String nom, String prenom, String dateNaissance, String mail, String codeAgence) {
        boolean resultat = false;
        try {
            prepStatConseiller = conn.prepareStatement(REQUETE_NOUV_CONSEILLER);
            prepStatConseiller.setString(1, id);
            prepStatConseiller.setString(2, nom);
            prepStatConseiller.setString(3, prenom);
            prepStatConseiller.setObject(5, Date.valueOf(dateNaissance));
            prepStatConseiller.setString(4, mail);
            prepStatConseiller.setString(6, codeAgence);
            prepStatConseiller.executeUpdate();
            resultat = true;
        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (prepStatConseiller != null) {
                try {
                    prepStatConseiller.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClientServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultat;
    }

}
