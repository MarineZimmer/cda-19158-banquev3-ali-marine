/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuthentificationServiceBdd {

    private PreparedStatement prepStatAuth;
    private static final String REQUETE_AUTHENTIFICATION = "SELECT * FROM employe e INNER JOIN authentification a ON a.id_emp = e.id_emp WHERE a.id_emp=? AND a.mot_passe=?";

    /**
     * service bdd d'authentification, interoge la bdd pour savoir si
     * l'authentification est correcte
     *
     * @param login : le loggin à tester
     * @param motDePasse : le mot de passe à tester
     * @param conn : la connexion à la bdd
     * @return : le resultat de la requete (les informations de la personne
     * authentifiée, si l'authentification est correcte, null si un problème a été rencontré
     */
    public ResultSet authentification(String login, String motDePasse, Connection conn) {
        try {
            prepStatAuth = conn.prepareStatement(REQUETE_AUTHENTIFICATION);
            prepStatAuth.setString(1, login);
            prepStatAuth.setString(2, motDePasse);
            return prepStatAuth.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(AuthentificationServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
