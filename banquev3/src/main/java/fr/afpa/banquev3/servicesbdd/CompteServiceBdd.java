/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.servicesbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CompteServiceBdd {

    private PreparedStatement prepStatCompte;
    private PreparedStatement prepStatCompte2;
    private PreparedStatement prepStatClientActif;
    private static final String REQUETE_RECHERCHE_COMPTE = "SELECT co.*,t.libelle FROM compte co JOIN type_compte t ON co.id_type=t.id_type JOIN client c on c.id_client=co.id_client WHERE nom LIKE ? AND c.id_client LIKE ? AND COALESCE(numero_compte,'') LIKE ? ";
    private static final String REQUETE_VERIFICATION_NOMBRE_COMPTE = "select count(*) from compte where id_client = ? and actif=true;";
    private static final String REQUETE_VERIFICATION_CLIENT_ACTIF = "select * from client where id_client = ? and actif=true;";
    private static final String REQUETE_NOUV_COMPTE = "insert into compte values (?, ?, ?, true, 25, ?, ?);";

    /**
     * service bdd qui interooge la BDD pour une recherche Comptes dont le nom
     * du propriétaire du compte et l'id du propriétaire du compte et le numéro
     * de compte commencent par les champs renseignés en paramètres.
     *
     * @param conn : la connection à la BDD
     * @param nom : le nom du propriétaire du compte à rechercher
     * @param idClient : l'id du propriétaire du compte à rechercher
     * @param numeroCompte : le numero de compte à rechercher
     * @return un ResultSet correspondant au resultat de la recherche
     */
    public ResultSet rechercheCompteClient(Connection conn, String nom, String idClient, String numeroCompte) {

        try {
            prepStatCompte = conn.prepareStatement(REQUETE_RECHERCHE_COMPTE, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
          prepStatCompte.setString(1, nom + "%");
            prepStatCompte.setString(2, idClient + "%");
            prepStatCompte.setString(3, numeroCompte + "%");
            return prepStatCompte.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CompteServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * service d'insertion d'un nouveau compte en BDD
     *
     * @param conn : connection à la bdd
     * @param numCompte que l'on souhaite ajouter
     * @param solde à l'ouverture du compte
     * @param decouvert autorisé ou non
     * @param id_type : type de compte (courant, PEL, Livret A)
     * @param id_client auquel on souhaite ajouter le nouveau compte
     * @return true si l'insertion a été effectuée avec succès, false sinon
     * @throws SQLException
     */
    public boolean creationCompte(Connection conn, String numCompte, String solde, String decouvert, String id_type, String id_client) {
        boolean resultat = false;
        try {
            prepStatCompte2 = conn.prepareStatement(REQUETE_VERIFICATION_NOMBRE_COMPTE);
             prepStatClientActif=conn.prepareStatement(REQUETE_VERIFICATION_CLIENT_ACTIF, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            
            prepStatCompte2.setString(1, id_client);
            prepStatClientActif.setString(1, id_client);
            ResultSet res = prepStatCompte2.executeQuery();
            ResultSet resClientActif = prepStatClientActif.executeQuery();
            if (res.next() && res.getInt(1) >= 3) {
                return false;
            }
            if(!resClientActif.next()){
                return false;
            }
            prepStatCompte = conn.prepareStatement(REQUETE_NOUV_COMPTE);
            prepStatCompte.setString(1, numCompte);
            prepStatCompte.setFloat(2, Float.parseFloat(solde));
            prepStatCompte.setBoolean(3, Boolean.parseBoolean(decouvert));
            prepStatCompte.setInt(4, Integer.parseInt(id_type));
            prepStatCompte.setString(5, id_client);
            prepStatCompte.executeUpdate();
            resultat = true;
        } catch (SQLException | IllegalArgumentException ex) {
            Logger.getLogger(CompteServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CompteServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (prepStatCompte != null) {
                try {
                    prepStatCompte.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CompteServiceBdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultat;
    }
}
