package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.AgenceDto;

public class AgenceService {

	/**
	 * service de création d'une nouvelle agence
	 * 
	 * @param codeAgence de la nouvelle agence (3 chiffres)
	 * @param nomAgence de la nouvelle agence
	 * @param adresseAgence de la nouvelle agence
	 * @return  true si la création a été effectuée avec succès, false sinon
	 */
	 public boolean creationAgence(String codeAgence, String nomAgence, String adresseAgence) {
	        AgenceDto agenceDto = new AgenceDto();
	        return agenceDto.creationAgence(codeAgence, nomAgence, adresseAgence);
	    }
}
