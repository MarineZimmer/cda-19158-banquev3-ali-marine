/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

public class OperationClientService {

    /**
     * traitement du choix de l'opération client à effectuer de l'utilisateur
     * (informations client, consulter comptes, consulter opérations comptes,
     * relevé compte, alimentation compte, retrait, virement)
     *
     * @param choix : le choix de l'utilisateur (0:formations client,
     * 1:consulter comptes, 2:consulter opérations comptes, 3:relevé compte,
     * 4:alimentation compte, 5:retrait, 6:virement)
     * @param numeroCompte : numero du compte de l'opération
     * @param idClient : l'identifiant du client à l'origine de l'opération
     * @param montant : le montant de la transaction (pour l'ajout, le retrait
     * et le virement)
     * @param compteDestinataire : numero du compte du destinataire pour le
     * virement
     * @param idClientDestinataire : l'identifiant du client destinataire pour
     * le virement
     * @param dateDebut : la date de debut pour le relevé
     * @param dateFin : la date de fin pour le relevé
     * @return true si l'opération a été effectuée avec succès, false sinon
     */
    public boolean traitementChoixOperationClient(int choix, String numeroCompte, String idClient, String montant, String compteDestinataire, String idClientDestinataire, String dateDebut, String dateFin) {
        TransactionsService transactionsService = new TransactionsService();
        ClientService clientService = new ClientService();
        CompteService compteService = new CompteService();
        switch (choix) {
            case 0:
                return clientService.creationFicheClient(idClient);
            case 1:
                return compteService.affichageInfoCompte(idClient, numeroCompte);
            case 2:
                return compteService.consulterOperationsCompte(idClient, numeroCompte);
            case 3:
                return compteService.imprimerReleveCompte(numeroCompte, idClient, dateDebut, dateFin);
            case 4:
                return transactionsService.effectuerTransaction(1, numeroCompte, idClient, montant, null, null);
            case 5:
                return transactionsService.effectuerTransaction(2, numeroCompte, idClient, montant, null, null);
            case 6:
                return transactionsService.effectuerTransaction(3, numeroCompte, idClient, montant, compteDestinataire, idClientDestinataire);
            default:
                return false;
        }
    }
}
