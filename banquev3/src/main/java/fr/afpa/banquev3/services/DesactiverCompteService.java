package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.DesactiverCompteDto;

public class DesactiverCompteService {

	/**
	 * service de désactivation d'un compte 
	 * 
	 * @param numeroCompte que l'on souhaite désactiver
	 * @return true si la modification s'est effectué avec succès, false sinon
	 */
	
	public boolean desactiverCompte(String numeroCompte) {
        DesactiverCompteDto desactivationCompteDto = new DesactiverCompteDto();
        return desactivationCompteDto.desactiverCompte(numeroCompte);
    }
	 
	
	
}
