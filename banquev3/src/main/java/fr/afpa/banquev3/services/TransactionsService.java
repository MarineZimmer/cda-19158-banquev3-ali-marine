/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.TransactionsDto;

public class TransactionsService {

    /**
     * Service de transactions (alimentation, retrait, virement)
     *
     * @param typeTransaction 1 : alimentation, 2 : retrait, 3 : virement
     * @param numeroCompte : numero du compte qui effectue la transaction
     * @param idClient : id du client qui effectue la transaction
     * @param montant : montant de la transaction
     * @param compteDestinataire : compte destinataire pour le virement
     * @param idClientDestinataire l'identifiant du client destinataire pour le
     * virement
     * @return true si la transaction s'est effectué correctement
     */
    public boolean effectuerTransaction(int typeTransaction, String numeroCompte, String idClient, String montant, String compteDestinataire, String idClientDestinataire) {
        TransactionsDto transactionsDto = new TransactionsDto();
        switch (typeTransaction) {
            case 1:
                return transactionsDto.alimentationCompte(numeroCompte, idClient, montant);
            case 2:
                return transactionsDto.retraitArgent(numeroCompte, idClient, montant);
            case 3:
                return transactionsDto.virement(numeroCompte, idClient, montant, compteDestinataire, idClientDestinataire);
            default:
                return false;
        }
    }

}
