/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.ClientDto;
import fr.afpa.banquev3.dto.CompteDto;
import fr.afpa.banquev3.entites.Client;
import fr.afpa.banquev3.entites.Compte;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ClientService {

    /**
     * service de création d'un client
     *
     * @param id : l'id du client(2 majuscules et 6 chiffres)
     * @param nom : le nom du client
     * @param prenom : le prenom du client
     * @param dateNaissance : la date de naissance du client
     * @param mail : le mail du client
     * @param idConseiller : l'identifiant de son conseiller atitré
     * @return true si la création a été effectuée avec succès, false sinon
     */
    public boolean creationClient(String id, String nom, String prenom, String dateNaissance, String mail, String idConseiller) {
        ClientDto clientDto = new ClientDto();
        return clientDto.creationClient(id, nom, prenom, dateNaissance, mail, idConseiller);
    }

    /**
     * service qui envoie une demande de recherche clients au ClientDto
     * (recherche les clients dont le nom et l'id et le numéro de compte
     * commencent par les champs renseignés ) et reçoit une liste de client du
     * dto et crée un TableModel pour renvoyer le resultat de la recherche à
     * l'ihm
     *
     * @param nom : le nom à rechercher
     * @param idClient : l'id à rechercher
     * @param numeroCompte : le numero de compte à rechercher
     * @return un TableModel ayant comme données les informations des Clients
     * recherchés, et le nom des colonnes associées
     */
    public TableModel rechercheClient(String nom, String idClient, String numeroCompte) {
        ClientDto clientDto = new ClientDto();
        List<Client> listeClient = clientDto.rechercheClient(nom, idClient, numeroCompte);
        Object[] nomColonnes = {"id client", "nom", "prenom", "mail", "date de naissance", "actif", "id conseiller"};
        Object[][] donnees = new Object[listeClient.size()][nomColonnes.length];
        int indiceLigne = 0;
        for (Client client : listeClient) {
            donnees[indiceLigne][0] = client.getLogin();
            donnees[indiceLigne][1] = client.getNom();
            donnees[indiceLigne][2] = client.getPrenom();
            donnees[indiceLigne][3] = client.getMail();
            donnees[indiceLigne][4] = client.getDateNaissance();
            donnees[indiceLigne][5] = client.isActif();
            donnees[indiceLigne][6] = client.getIdConseiller();
            indiceLigne++;
        }
        return new DefaultTableModel(donnees, nomColonnes);
    }
    
    /**
     * service de modification de la domiciliation bancaire d'un client
     * 
     * @param idClient à modifier la domiciliation
     * @param idConseiller : le nouveau conseiller auquel le client sera rattaché
     * @return true si la modification a été effectuée avec succès, false sinon
     */
	public boolean modifierDomiciliationBancaire(String idClient, String idConseiller) {
        ClientDto modifDomicilClient = new ClientDto();
        return modifDomicilClient.modifDomiciliationClient(idClient, idConseiller);
    }

    /**
     * service de création d'une fiche client 
     * @param idClient : l'id du client
     * @return true si la fiche a été créé false sinon
     */
    public boolean creationFicheClient(String idClient) {
        ClientDto clientDto = new ClientDto();
        CompteDto compteDto = new CompteDto();
        FichierService fichierService = new FichierService();
        Client client = null;
        StringBuilder infoClient = new StringBuilder();
        List<Client> liste = clientDto.rechercheClient("", idClient, "");
        List<Compte> listeCompte =  compteDto.rechercheCompteClient("", idClient, "");
        for (Client client1 : liste) {
            if(client1.getLogin().equals(idClient)){
                client=client1;
                break;
            }
        }
        if (client != null) {
            for (Compte compte : listeCompte) {
                client.getListeComptes().put(compte.getNumeroCompte(), compte);
            }
            
            infoClient.append(String.format("%50s", "Fiche client"));
            infoClient.append("\n\nNuméro client : ").append(client.getLogin());
            infoClient.append("\nNom : ").append(client.getNom());
            infoClient.append("\nPrenom : " ).append( client.getPrenom());
            infoClient.append(
                    "\nDate de naissance :" ).append( client.getDateNaissance().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            infoClient.append("\n\n_____________________________________________________________________________________");
            infoClient.append("\nListe de compte");
            infoClient.append("\n_____________________________________________________________________________________");
            infoClient.append(String.format("%30s", "\nNumero de compte"));
            infoClient.append(String.format("%30s", "solde"));
            infoClient.append("\n_____________________________________________________________________________________");

            for (Map.Entry<String, Compte> compte : client.getListeComptes().entrySet()) {
                if (compte.getValue().isActif()) {
                    infoClient.append(String.format("%30s", "\n" + compte.getKey()));
                    infoClient.append(String.format("%40s", compte.getValue().getSolde() + " euros"));
                    if (compte.getValue().getSolde() > 0) {
                        infoClient.append(String.format("%20s", ":-)"));
                    } else {
                        infoClient.append(String.format("%20s", ":-("));
                    }
                }
            }
            infoClient.append("\n_____________________________________________________________________________________");
            return fichierService.ecritureFichier("fiche_client\\client"+client.getLogin()+LocalDate.now()+".txt", infoClient.toString(), false);
        }
        return false;
    }
    
    /**
     * service de modification des informations du client
     * 
     * @param id du client auquel on souhaite modifier ces infos
     * @param nom du client à modifier les infos
     * @param prenom du client à modifier les infos
     * @param dateNaissance du client à modifier les infos
     * @param mail du client à modifier les infos
     * @return true si la modification a été effectuée avec succès, false sinon
     */
    
    public boolean modifierInformationClient(String id, String nom, String prenom, String dateNaissance, String mail) {
        ClientDto modifInfoClient = new ClientDto();
        return modifInfoClient.modifInformationClient(id, nom, prenom, dateNaissance, mail);
    }
    
    
    /**
     * service de désactivation d'un client et de ses comptes
     * @param idClient : l'identifiant du client
     * @return true si le client et ses comptes ont été désactivés 
     */
    public boolean desactiverClient(String idClient){
        ClientDto clientDto = new ClientDto();
        return clientDto.desactiverClient(idClient);
    }
}
