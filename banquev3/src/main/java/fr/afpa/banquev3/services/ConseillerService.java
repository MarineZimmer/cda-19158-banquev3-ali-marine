package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.ConseillerDto;

public class ConseillerService {

	/**
	 * service de création d'un conseiller
	 * 
	 * @param id  du nouveau conseiller (CO et 4 chiffres)
     * @param nom du nouveau conseiller
     * @param prenom du nouveau conseiller
     * @param dateNaissance du nouveau conseiller
     * @param mail du nouveau conseiller
     * @param codeAgence : agence à laquelle le nouveau conseiller va être rattaché
	 * @return true si la création a été effectuée avec succès, false sinon
	 */
	
	 public boolean creationConseiller(String id, String nom, String prenom, String dateNaissance, String mail, String codeAgence) {
	        ConseillerDto conseillerDto = new ConseillerDto();
	        return conseillerDto.creationConseiller(id, nom, prenom, dateNaissance, mail, codeAgence);
	    }
}
