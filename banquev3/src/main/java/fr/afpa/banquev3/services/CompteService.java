/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.ClientDto;
import fr.afpa.banquev3.dto.CompteDto;
import fr.afpa.banquev3.dto.TransactionsDto;
import fr.afpa.banquev3.entites.Client;
import fr.afpa.banquev3.entites.Compte;


import fr.afpa.banquev3.entites.Operation;
import java.sql.Date;
import java.time.LocalDate;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class CompteService {

    /**
     * service qui envoie une demande de recherche compte au CompteDto
     * (recherche les comptes dont le nom du propriètaire et l'id du
     * propriètaire et le numéro de compte commencent par les champs renseignés)
     * et reçoit une liste de compte du dto et crée un TableModel pour renvoyer
     * le resultat de la recherche à l'ihm
     *
     * @param nom : le nom du propriètaire du compte à rechercher
     * @param idClient : l'id du propriètaire du compte à rechercher
     * @param numeroCompte : le numero de compte à rechercher
     * @return un TableModel ayant comme données les informations des Comptes
     * recherchés, et le nom des colonnes associées
     */
    public TableModel rechercheCompteClient(String nom, String idClient, String numeroCompte) {
        CompteDto compteDto = new CompteDto();
        List<Compte> listeCompte = compteDto.rechercheCompteClient(nom, idClient, numeroCompte);
        Object[] nomColonnes = {"numero compte", "id client", "type", "solde", "decouvert", "actif"};
        Object[][] donnees = new Object[listeCompte.size()][nomColonnes.length];
        int indiceLigne = 0;
        for (Compte compte : listeCompte) {
            donnees[indiceLigne][0] = compte.getNumeroCompte();
            donnees[indiceLigne][1] = compte.getIdClient();
            donnees[indiceLigne][2] = compte.getType();
            donnees[indiceLigne][3] = compte.getSolde();
            donnees[indiceLigne][4] = compte.isDecouvert();
            donnees[indiceLigne][5] = compte.isActif();
            indiceLigne++;
        }
        return new DefaultTableModel(donnees, nomColonnes);
    }

    /**
     * service de création d'un nouveau compte
     *
     * @param numCompte que l'on souhaite ajouter
     * @param solde à l'ouverture du compte
     * @param decouvert autorisé ou non
     * @param id_type : type de compte (courant, PEL, Livret A)
     * @param id_client auquel on souhaite ajouter le nouveau compte
     * @return true si la création a été effectuée avec succès, false sinon
     */
    public boolean creationCompte(String numCompte, String solde, String decouvert, String id_type, String id_client) {
        CompteDto compteDto = new CompteDto();
            return compteDto.creationCompte(numCompte, solde, decouvert, id_type, id_client);
        
    }

    /**
     * Service qui imprime un releve de compte (enregitre dans un fichier txt
     * (releve_compte)
     *
     * @param numeroCompte : le numéro de compte
     * @param idClient : l'identifiant du client
     * @param dateDebut : la date de début
     * @param dateFin : la date de fin
     * @return  true si le fichier contenant le releve de compte à été généré
     */
    public boolean imprimerReleveCompte(String numeroCompte, String idClient, String dateDebut, String dateFin) {
        CompteDto compteDto = new CompteDto();
        ClientDto clientDto = new ClientDto();
        TransactionsDto transactionDto = new TransactionsDto();
        FichierService fs = new FichierService();
        StringBuilder ecritureFichier = new StringBuilder();
        Compte compte = null;
        Client client = null;

        try {
            if ("".equals(dateFin)) {
                dateFin = Date.valueOf(LocalDate.now()).toString();
            }
            if (Date.valueOf(dateDebut).after(Date.valueOf(dateFin)) || Date.valueOf(dateDebut).after(Date.valueOf(LocalDate.now()))) {
                return false;
            }
        } catch (Exception ex) {
            Logger.getLogger(CompteService.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Client> listeClient = clientDto.rechercheClient("", idClient, numeroCompte);
        List<Compte> listeCompte = compteDto.rechercheCompteClient("", idClient, numeroCompte);
        List<Operation> listeOperation = transactionDto.rechercheTransaction(numeroCompte, dateDebut, dateFin);

        for (Compte compte1 : listeCompte) {
            if (compte1.getNumeroCompte().equals(numeroCompte)) {
                compte = compte1;
            }
        }
        for (Client client1 : listeClient) {
            if (client1.getLogin().equals(idClient)) {
                client = client1;
            }
        }

        if (client != null && compte != null && compte.isActif() && listeOperation != null) {
            ecritureFichier.append("----------------- Releve de compte numero ").append(compte.getNumeroCompte()).append(" ----------------------------\n\n");
            ecritureFichier.append("Date du releve de compte : du ").append(dateDebut).append(" au ").append(dateFin);
            ecritureFichier.append("\n\n");
            ecritureFichier.append("Client : ").append(client.getNom()).append(" ").append(client.getPrenom()).append("\nIdentifiant : ").append(client.getLogin());
            ecritureFichier.append("\n\n");

            ecritureFichier.append(String.format("%20s", "date |"));
            ecritureFichier.append(String.format("%20s", "type |"));
            ecritureFichier.append(String.format("%20s", "valeur|")).append("\n");

            for (Operation operation : listeOperation) {
                ecritureFichier.append(String.format("%20s", operation.getDate() + " |"));
                ecritureFichier.append(String.format("%20s", operation.getType() + " |"));
                ecritureFichier.append(String.format("%20s", operation.getMontant() + " |")).append("\n");

            }
            ecritureFichier.append("\n Solde du compte : ").append(compte.getSolde());
            return fs.ecritureFichier("releve_compte\\releve_compte_" + compte.getNumeroCompte() + ".txt", ecritureFichier.toString(), false);
        }
        return false;
    }

    /**
     *  service de consultation de l'ensemble des operations d'un compte du client
     * @param idClient : l'identifiant du client
     * @param numeroCompte : le numéro de compte
     * @return  true si la consultation des opérations à été effectuée, false sinon
     */
    public boolean consulterOperationsCompte(String idClient, String numeroCompte) {
        CompteDto compteDto = new CompteDto();
        TransactionsDto transactionDto = new TransactionsDto();

        Compte compte = null;

        List<Compte> listeCompte = compteDto.rechercheCompteClient("", idClient, numeroCompte);
        List<Operation> listeOperation = transactionDto.rechercheTransaction(numeroCompte, "2010-01-01", "");

        for (Compte compte1 : listeCompte) {
            if (compte1.getNumeroCompte().equals(numeroCompte)) {
                compte = compte1;
            }
        }

        if (compte != null && compte.isActif() && listeOperation != null) {
            StringBuilder operations = new StringBuilder();
            operations.append(
                    "----------------- compte numero ").append(compte.getNumeroCompte()).append(" ----------------------------\n\n");
            operations.append("\n\n");

            operations.append(String.format("%20s", "date |"));
            operations.append(String.format("%20s", "type |"));
            operations.append(String.format("%20s", "valeur|")).append("\n");

            for (Operation operation : listeOperation) {
                operations.append(String.format("%20s", operation.getDate() + " |"));
                operations.append(String.format("%20s", operation.getType() + " |"));
                operations.append(String.format("%20s", operation.getMontant() + " |")).append("\n");
            }

            operations.append("------------------------------------------------------------------------------------\n\n");
            JOptionPane.showMessageDialog(null, operations);
            return true;
        }
        return false;
    }

    /**
     * Service d'affichage des informations d'un compte
     * @param idClient : l'identifiant du client
     * @param numeroCompte : le numéro de compte
     * @return true si l'affichage à été effectuée, false sinon
     */
    public boolean affichageInfoCompte(String idClient, String numeroCompte) {

        CompteDto compteDto = new CompteDto();

        Compte compte = null;

        List<Compte> listeCompte = compteDto.rechercheCompteClient("", idClient, numeroCompte);

        for (Compte compte1 : listeCompte) {
            if (compte1.getNumeroCompte().equals(numeroCompte)) {
                compte = compte1;
            }
        }
        if (compte != null) {
            StringBuilder infoCompte = new StringBuilder();
            infoCompte.append("-------------------- Infos Compte  ---------------------------");
            infoCompte.append("\nNumero de compte : ").append(compte.getNumeroCompte());
            infoCompte.append("\nType de compte : ").append(compte.getType());
            infoCompte.append("\nsolde : ").append(compte.getSolde());
            infoCompte.append("\ndecouvert autorise : ").append(compte.isDecouvert());
            infoCompte.append("\nactif : ").append(compte.isActif());
            infoCompte.append("\nfrais  : ").append(calculFrais(compte));
            infoCompte.append("\n-----------------------------------------------------------------");
            JOptionPane.showMessageDialog(null, infoCompte);
            return true;
        }
        return false;
    }

    /**
     * service de calcul des frais d'un compte
     * @param compte : le compte dont il faut calculer les frais
     * @return les frais du compte
     */
    public float calculFrais(Compte compte) {
        if ("LIVRET A".equals(compte.getType())) {
            return compte.getFrais() + (0.1f * compte.getSolde());
        }
        if ("PLAN EPARGNE LOGEMENT".equals(compte.getType())) {
            return compte.getFrais() + (0.025f * compte.getSolde());
        }
        return compte.getFrais();
    }
}
