/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.services;

import fr.afpa.banquev3.dto.AuthentificationDto;
import fr.afpa.banquev3.entites.Administrateur;


public class AuthentificationService {

    /**
     * service d'authentification
     * @param login : le login de la personne qui s'authentifie
     * @param motDePasse : le mot de passe de la personne qui s'authentifie
     * @return l'administrateur authentifier si le mot de passe et login sont corrects, null sinon
     */
    public Administrateur authenfication(String login, String motDePasse) {
        AuthentificationDto authentificationDto = new AuthentificationDto();
        return authentificationDto.authentification(login,motDePasse);
        }
    
}
