package fr.afpa.banquev3.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FichierService {

    /**
     * Service permettant la sauvegarde d'une chaine de caractères dans un
     * fichier 
     *
     * @param cheminFichier : le chemin du fichier
     * @param enregistrement : la ligne à écrire
     * @param ecriture : true si on rajoute la ligne au fichier, false si on
     * écrase le fichier
     */
    public boolean ecritureFichier(String cheminFichier, String enregistrement, boolean ecriture) {
        boolean retour = true;
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            File fichier = new File(cheminFichier);
            File dir = fichier.getParentFile();
            dir.mkdirs();
            fw = new FileWriter(cheminFichier, ecriture);
            bw = new BufferedWriter(fw);
            bw.write(enregistrement);
        } catch (IOException ex) {

            Logger.getLogger(FichierService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        	if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    retour = false;
                    Logger.getLogger(FichierService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    retour = false;
                    Logger.getLogger(FichierService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
        return retour;
    }

}
