package fr.afpa.banquev3.entites;

import java.time.LocalDate;

;

@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class Personne {

    private String login;
    private String nom;
    private String prenom;
    private LocalDate dateNaissance;
    private String mail;

}
