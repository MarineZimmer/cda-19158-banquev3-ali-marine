package fr.afpa.banquev3.entites;

import java.time.LocalDate;

@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
 public class Administrateur extends Personne  {

    public Administrateur(String nom, String prenom, LocalDate dateNaissance, String mail, String login) {
        super(login, nom, prenom, dateNaissance, mail);
    }
	
}
