package fr.afpa.banquev3.entites;

@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class Compte {

    private String numeroCompte;
    private String idClient;
    private String type;
    private float solde;
    private boolean decouvert;
    private boolean actif;
    private float frais;

}
