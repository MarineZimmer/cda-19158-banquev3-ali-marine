package fr.afpa.banquev3.entites;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class Client extends Personne {

    private Map<String, Compte> listeComptes;
    private boolean actif;
    private String idConseiller;

    public Client(String login, String nom, String prenom, String mail, LocalDate dateNaissance, boolean actif, String idConseiller) {
        super(login, nom, prenom, dateNaissance, mail);
        this.idConseiller = idConseiller;
        this.actif = actif;
        this.listeComptes = new HashMap<String, Compte>();
    }
}
