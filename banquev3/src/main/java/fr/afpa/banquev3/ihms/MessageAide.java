/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.banquev3.ihms;


public class MessageAide {
     protected static final String MESSAGE = "Contraintes de saisie : \n" +
"  - Identifiant : \n" +
"	- Identifiant client 2 Majuscules + 6 chiffres\n" +
"	- Identifiant conseiller CO + 4 chiffres\n" +
"	- Identifiant agence 3 chiffres\n" +
"	- Numéro de compte 10 chiffres\n" +
"Lors de la création, si l'identifiant existe déjà, un message d'erreur s'affiche\n" +
"	\n" +
"  - Date format aaaa-mm-jj\n" +
"\n" +
"  - Montant (alimentation, retrait, virement) doit être strictement positif \n" +
"\n" +
"\n" +
"- Champs nécessaires pour les opérations client : \n" +
"	- Informations Client (enregistrement de la fiche client dans un fichier .txt dans le dossier fiche_client)\n" +
"	  Champs nécessaires : identifiant client\n" +
"\n" +
"	- Consulter Compte\n" +
"	 Champs nécessaires :  numéro de compte\n" +
"\n" +
"	- Consulter opérations compte\n" +
"	  Champs nécessaires :  numéro de compte\n" +
"\n" +
"	- Relevé compte (enregistrement de la fiche client dans un fichier .txt dans le dossier releve_compte)\n" +
"	 Champs nécessaires : identifiant client, numéro de compte, date début, date fin (optionnel)\n" +
"\n" +
"	- Alimenter un compte d'un client\n" +
"	 Champs nécessaires : identifiant client, numéro de compte, montant \n" +
"\n" +
"	- Retirer de l'argent d'un compte d'un client\n" +
"	 Champs nécessaires : identifiant client, numéro de compte, montant \n" +
" \n" +
"	- Faire un virement d'un compte d'un client à un autre \n" +
"	  Champs nécessaires : identifiant client, numéro de compte, montant ,identifiant client destinataire, numéro de compte destinataire\n" +
"\n" +
"";
    
}
