Projet : BanqueV3
Auteurs : Ali, Marine


Technologies et outils utilis�s : Java / Maven/ Lombok / Swing / Gitkraken / Bitbucket / Gliffy  / JMerise / PgAdmin 4 / DBeaver

D�marche � suivre : 
1) Sur pgAdmin ou autre syst�me de gestion de base de donn�es, cr�er une nouvelle base de donn�es PostgreSQL(avec l'user postgres de pr�f�rence) et ex�cuter du script de creationBdd.sql (il se trouve dans le dossier documentation/BDD)

2) Cr�er un fichier configBdd.properties, Mettez dans ce fichier la configuration (user,password,url) afin de vous connecter � la base donn�es, exemple : 
user=postgres
password=admin
url=jdbc:postgresql://localhost:5432/banque

3)Mettre le fichier configBdd.properties dans le m�me dossier que banqueV3.jar ou cr�er une variable d'environnement configBDD et mettre la chemin d'acc�s au fichier properties


1) Sur le r�pertoire C cr�er l�arborescence suivante
 banqueV3.jar se trouve dans le dossier BanqueAliMarine

	ENV
   	|-----BanqueAliMarine
		|-------banqueV3.jar
		|-------configBdd.properties
		

Pour lancer l'application : 
1) Ouvrir l�invite de commande (cmd dans la barre de recherche).
2) D�marrage de l�application, taper les commandes : 
cd C:\ENV\BanqueAliMarine
java -jar banqueV3.jar

Ou double clique sur banqueV3.jar


Fonctionnement de l'application : 



Lors de la cr�ation de la base de donn�es, une banque est initialis�e, elle contient : 
    Agence(codeAgence = 001, nom=agence 1, adresse = LILLE)
 	|
	| -Conseiller(id=CO0001, nom=CONS1, prenom=Cons1,  date de naissance=1968-11-12 , mail=cons1@gmail.com) 
	|	|
	|	|-Client(identifiant=CC000001 nom=CLIENT1, prenom=client1,  date de naissance=1988-11-12 , mail=client1@gmail.com,actif=true)
	|		|
	|		|-CompteCourant(numeroCompte=00000000001,decouvert=true,solde=1000)
	|		|-LivretA(numeroCompte=00000000002,decouvert=false,solde=500)
	|
    Agence(codeAgence = 002, nom=agence 2, adresse = ROUBAIX)
	|
	| -Conseiller(id=CO0002, nom=CONS2, prenom=Cons2,  date de naissance=1968-03-14 , mail=cons2@gmail.com) 
	|	|
	|	|-Client(identifiant=CC000002 nom=CLIENT2, prenom=client2,  date de naissance=1998-11-12 , mail=client2@gmail.com,actif=true)
	|		|
	|		|-CompteCourant(numeroCompte=00000000003,decouvert=true,solde=1800)
	

Une page d'authentification apparrait, pour vous authentifier il faut mettre login=ADM00 et mot de passe=admim, si l'authentification est r�usssite, 
La page principale s'affiche, sur cette interface l'administrateur peut : 

 - Faire une recherche de client et de compte via le nom du client, et/ou son identifiant, et/ou son num�ro de compte la recherche est sensible � la cast. 
   La recherche affiche tous les clients et leurs comptes correspondant au crit�re de recherche (elle compare la valeur entr�e avec les valeurs en BDD si la valeur entr�e correspond au d�but de la valeur en BDD,elle l'affiche.

- Cr�er une agence, un conseiller, un client, un compte, modifier les informations d'un client, changer la domiciliation d'un client.

- D�sactiver un client (d�sactivation de tous ses comptes en m�me temps) ou un compte.

- Op�rations Client : 
	- Consulter les informations d'un client (enregistrement de la fiche client dans un fichier .txt dans le dossier fiche_client)
	  Champs n�cessaires : identifiant client

	-Consulter un compte d'un client
	 Champs n�cessaires :  num�ro de compte

	-Consulter les op�rations d' un compte
	  Champs n�cessaires :  num�ro de compte
	
	-Imprimer un relev� d�un compte donn� (enregistrement de la fiche client dans un fichier .txt dans le dossier releve_compte)
	 Champs n�cessaires : identifiant client, num�ro de compte, date d�but, date fin (optionnel)

	-Alimenter un compte d'un client
	 Champs n�cessaires : identifiant client, num�ro de compte, montant 

	-Retirer de l'argent d'un compte d'un client
	 Champs n�cessaires : identifiant client, num�ro de compte montant 

	-Faire un virement d'un compte d'un client � un autre 
	  Champs n�cessaires : identifiant client, num�ro de compte, montant ,identifiant client destinataire, num�ro de compte destinataire



Contraintes de saisie : 
  - Identifiant : 
	- Identifiant client 2 Majuscules + 6 chiffres
	- Identifiant conseiller CO + 4 chiffres
	- Identifiant agence 3 chiffres
	- Num�ro de compte 10 chiffres
Lors de la cr�ation, si l'identifiant existe d�j�, un message d'erreur s'affiche
	
  - Date format aaaa-mm-jj

  - Montant (alimentation, retrait, virement) doit �tre strictement positif 


Horaires de fonctionnement du programme : 24h/24, 7j/7



