﻿------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------



------------------------------------------------------------
-- Table: role_emp
------------------------------------------------------------
CREATE TABLE public.role_emp(
	id_role   INT  NOT NULL ,
	libelle   VARCHAR (15) NOT NULL  ,
	CONSTRAINT role_emp_PK PRIMARY KEY (id_role)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: type_compte
------------------------------------------------------------
CREATE TABLE public.type_compte(
	id_type   INT  NOT NULL ,
	libelle   VARCHAR (30) NOT NULL  ,
	CONSTRAINT type_compte_PK PRIMARY KEY (id_type)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: type_operation
------------------------------------------------------------
CREATE TABLE public.type_operation(
	id_type_operation   INT  NOT NULL ,
	libelle             VARCHAR (10) NOT NULL  ,
	CONSTRAINT type_operation_PK PRIMARY KEY (id_type_operation)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: client
------------------------------------------------------------
CREATE TABLE public.client(
	id_client        VARCHAR (8) NOT NULL  ,
	nom              VARCHAR (30) NOT NULL ,
	prenom           VARCHAR (30) NOT NULL ,
	mail             VARCHAR (30) NOT NULL ,
	date_naissance   DATE  NOT NULL ,
	actif            BOOL  NOT NULL ,
	id_emp           VARCHAR (6) NOT NULL  ,
	CONSTRAINT client_PK PRIMARY KEY (id_client)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: employe
------------------------------------------------------------
CREATE TABLE public.employe(
	id_emp           VARCHAR (6) NOT NULL ,
	nom              VARCHAR (30) NOT NULL ,
	prenom           VARCHAR (30) NOT NULL ,
	mail             VARCHAR (30) NOT NULL ,
	date_naissance   DATE  NOT NULL ,
	id_role          INT  NOT NULL ,
	code_agence      VARCHAR (3)    ,
	CONSTRAINT employe_PK PRIMARY KEY (id_emp)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: agence
------------------------------------------------------------
CREATE TABLE public.agence(
	code_agence   VARCHAR (3) NOT NULL,
	nom_agence    VARCHAR (20) NOT NULL ,
	adresse       VARCHAR (100) NOT NULL ,
	id_emp        VARCHAR (6) NOT NULL  ,
	CONSTRAINT agence_PK PRIMARY KEY (code_agence)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: authentification
------------------------------------------------------------
CREATE TABLE public.authentification(
	id_auth     INT  NOT NULL ,
	mot_passe   VARCHAR (10) NOT NULL ,
	id_emp      VARCHAR (6) NOT NULL  ,
	CONSTRAINT authentification_PK PRIMARY KEY (id_auth)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: compte
------------------------------------------------------------
CREATE TABLE public.compte(
	numero_compte         VARCHAR (10) NOT NULL ,
	solde           FLOAT  NOT NULL ,
	decouvert       BOOL  NOT NULL ,
	actif           BOOL  NOT NULL ,
	frais           FLOAT  NOT NULL ,
	id_type         INT  NOT NULL ,
	id_client       VARCHAR (8) NOT NULL  ,
	CONSTRAINT compte_PK PRIMARY KEY (numero_compte)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: effectuer_transaction
------------------------------------------------------------
CREATE TABLE public.effectuer_transaction(
	id_client             VARCHAR (8) NOT NULL ,
	numero_compte         VARCHAR (10) NOT NULL ,
	id_type_operation     INT  NOT NULL ,
	id_operation          SERIAL  NOT NULL ,
	date_operation        DATE  NOT NULL ,
	montant               FLOAT  NOT NULL ,
	compte_destinataire   VARCHAR (10)    ,
	CONSTRAINT effectuer_transaction_PK PRIMARY KEY (id_operation)
)WITHOUT OIDS;

------------------------------------------------------------
-- Contraintes clés étrangères
------------------------------------------------------------

ALTER TABLE public.client
	ADD CONSTRAINT client_employe0_FK
	FOREIGN KEY (id_emp)
	REFERENCES public.employe(id_emp) ON DELETE RESTRICT;

ALTER TABLE public.authentification
	ADD CONSTRAINT authentification_employe0_FK
	FOREIGN KEY (id_emp)
	REFERENCES public.employe(id_emp) ON DELETE RESTRICT;

ALTER TABLE public.employe
	ADD CONSTRAINT employe_role_emp0_FK
	FOREIGN KEY (id_role)
	REFERENCES public.role_emp(id_role) ON DELETE RESTRICT;

ALTER TABLE public.employe
	ADD CONSTRAINT employe_agence1_FK
	FOREIGN KEY (code_agence)
	REFERENCES public.agence(code_agence) ON DELETE RESTRICT;

ALTER TABLE public.agence
	ADD CONSTRAINT agence_employe0_FK
	FOREIGN KEY (id_emp)
	REFERENCES public.employe(id_emp) ON DELETE RESTRICT;

ALTER TABLE public.compte
	ADD CONSTRAINT compte_type_compte0_FK
	FOREIGN KEY (id_type)
	REFERENCES public.type_compte(id_type) ON DELETE RESTRICT;

ALTER TABLE public.compte
	ADD CONSTRAINT compte_client0_FK
	FOREIGN KEY (id_client)
	REFERENCES public.client(id_client) ON DELETE RESTRICT;


ALTER TABLE public.effectuer_transaction
	ADD CONSTRAINT compte_destinataire_FK
	FOREIGN KEY (compte_destinataire)
	REFERENCES public.compte(numero_compte) ON DELETE RESTRICT;

ALTER TABLE public.effectuer_transaction
	ADD CONSTRAINT compte_FK
	FOREIGN KEY (numero_compte)
	REFERENCES public.compte(numero_compte) ON DELETE RESTRICT;

ALTER TABLE public.effectuer_transaction
	ADD CONSTRAINT id_client_FK
	FOREIGN KEY (id_client)
	REFERENCES public.client(id_client) ON DELETE RESTRICT;

ALTER TABLE public.effectuer_transaction
	ADD CONSTRAINT id_type_operation_FK
	FOREIGN KEY (id_type_operation)
	REFERENCES public.type_operation(id_type_operation) ON DELETE RESTRICT;
	
------------------------------------------------------------
-- Contraintes format données (identifiant)
------------------------------------------------------------	
ALTER table employe
	ADD constraint identifiant_emp CHECK ((id_emp ~ E'^CO\\d{4}$' and id_role='2' and code_agence is not null)  OR (id_emp ~ E'^ADM\\d{2}$' and id_role='1' and code_agence is null));

ALTER table client
	ADD constraint identifiant_client CHECK (id_client ~ E'^[A-Z]{2}\\d{6}$'); 
	
ALTER table client
	ADD constraint identifiant_conseiller CHECK (id_emp ~ E'^CO\\d{4}$');

ALTER table agence
	ADD constraint code_agence CHECK (code_agence ~ E'^\\d{3}$');

ALTER table agence
	ADD constraint id_emp CHECK (id_emp ~ E'^ADM\\d{2}$');
	
ALTER table compte
	ADD constraint numero_compte CHECK (numero_compte ~ E'^\\d{10}$');

	
ALTER table compte
ADD constraint transaction_autorise CHECK ((compte.solde>=0 AND compte.decouvert='false') OR compte.decouvert='true' );

ALTER TABLE effectuer_transaction
ADD constraint montant_positif CHECK (effectuer_transaction.montant>0);

ALTER TABLE effectuer_transaction
ADD constraint compte_dest_virement CHECK ((effectuer_transaction.id_type_operation!='3' and effectuer_transaction.compte_destinataire is  null) OR (effectuer_transaction.id_type_operation='3' AND effectuer_transaction.compte_destinataire is not null ));
------------------------------------------------------------
-- Insertion données
------------------------------------------------------------
insert into role_emp values ('1', 'ADMINISTRATEUR');
insert into role_emp values ('2', 'CONSEILLER');
commit;

insert into type_compte values('1', 'COMPTE COURANT');
insert into type_compte values('2', 'LIVRET A');
insert into type_compte values('3', 'PLAN EPARGNE LOGEMENT');
commit;

insert into type_operation values('1','AJOUT');
insert into type_operation values('2', 'RETRAIT');
insert into type_operation values('3', 'VIREMENT');
commit;


insert into employe values ('ADM00', 'ADMIN', 'admin','admin@gmail.com','1960-01-12','1',null);
insert into authentification values('1','admin','ADM00');
commit;

insert into agence values('001','AGENCE 1', 'LILLE','ADM00');
insert into agence values('002','AGENCE 2', 'ROUBAIX','ADM00');
commit;

insert into employe values ('CO0001', 'CONS1', 'Cons1','cons1@gmail.com','1968-11-12','2','001');
insert into employe values ('CO0002', 'CONS2', 'Cons2','cons2@gmail.com','1968-03-14','2','002');
commit;


insert into client values ('CC000001', 'Client1', 'Client1','1988-11-12','1988-11-12',true, 'CO0001');
insert into client values ('CC000002', 'Client2', 'Client2','client2@gmail.com','1998-11-12',true, 'CO0002');
commit;

insert into compte values ('0000000001','1000',true, true,'25','1','CC000001');
insert into compte values ('0000000002','500',false, true,'25','2','CC000001');
insert into compte values ('0000000003','1800',true, true,'25','1','CC000002');
commit;





